This is a collection of web hacks (e.g. Javascript/CSS/HTML hacks, or Processing, and so on). I keep little hacks here from various places such as CodePen.

For public display, I add this as a [subrepo](https://github.com/ingydotnet/git-subrepo#readme) to my main website project [milosophical-me](https://gitlab.com/milohax/milosophical-me). Do a `git subrepo pull files/hax/` to update it there whenever new hacks are added.
